//
// Created by Kai Kuhlmann on 04.05.16.
//

#ifndef OOA_PR5_DIGRAPH_H
#define OOA_PR5_DIGRAPH_H


#include <string>
#include <set>
#include <queue>
#include <cmath>
#include <map>
#include "Node.h"
#include "Edge.h"
#include "Line.h"

using namespace std;

class GraphVisualizer;

class DiGraph {

private:
    vector<Node*> nodes;
    GraphVisualizer* visualizer;

public:
    void addNode(Node *node);
    void addEdge(string key1, string key2, float weight);
    void addEdge(Edge *edge);

    vector<Node*> getNeighbours(string key);
    vector<Edge*> getEdges(string key);
    vector<Node*> getNodes();
    Node* getNode(string key);

    vector<Edge*> dijkstraShortestPath(Node *start, Node *end);
    void setVisualizer(GraphVisualizer* graphviz);
    GraphVisualizer* getVisualizer();
};

void DiGraph::addNode(Node *node) {
    nodes.push_back(node);
}

void DiGraph::addEdge(string key1, string key2, float weight) {
    Node *node1 = this->getNode(key1);
    Node *node2 = this->getNode(key2);
    Edge *edge = new Line(node1, node2, weight);

    node1->setNewEdge(edge);
}

void DiGraph::addEdge(Edge *edge) {
    Node *node = edge->getStartNode();

    node->setNewEdge(edge);
}

vector<Node*> DiGraph::getNeighbours(string key) {
    vector<Node*> nbs;
    vector<Edge*> node_edges = getEdges(key);

    for (int i = 0; i < node_edges.size(); i++) {
        nbs.push_back(node_edges[i]->getEndNode());
    }

    return nbs;
}

vector<Edge*> DiGraph::getEdges(string key) {
    vector<Edge*> edges;

    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i]->getKey() == key) {
            edges = nodes[i]->getEdges();
        }
    }

    return edges;
}

vector<Node*> DiGraph::getNodes() {
    return nodes;
}

Node* DiGraph::getNode(string key) {
    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i]->getKey() == key) return nodes[i];
    }

    return nullptr;
}

struct comp {
    bool operator() (const pair<Node*, float> &lhs, const pair<Node*, float> &rhs) const {
        return lhs.second == rhs.second || lhs.second < rhs.second;
    }
};

vector<Edge*> DiGraph::dijkstraShortestPath(Node *start, Node *end) {
    vector<Edge*> result;
    set<pair<Node*, float>, comp> q;
    map<Node*, float> dist;
    map<Node*, Node *> prev;

    for (int i = 0; i < nodes.size(); i++) {
        if (nodes[i] == start) dist[start] = 0;
        else dist[nodes[i]] = numeric_limits<float>::infinity();

        q.insert(make_pair(nodes[i], dist[nodes[i]]));
    }

    while (!q.empty()) {
        Node *u = q.begin()->first;
        q.erase(q.begin());
        vector<Edge*> outEdges = u->getEdges();

        for (int i = 0; i < outEdges.size(); i++) {
            Node *v = outEdges[i]->getEndNode();

            float alternative = dist[u] + outEdges[i]->getWeight();

            if (alternative < dist[v]) {
                float index = dist[v];
                dist[v] = alternative;
                prev[v] = u;

                q.erase(make_pair(v, index));
                q.insert(make_pair(v, dist[v]));
            }
        }
    }

    Node *u = end;

    while (prev[u] != nullptr) {
        result.insert(result.begin(), prev[u]->getEdge(u));
        u = prev[u];
    }

    return result;
}

void DiGraph::setVisualizer(GraphVisualizer *graphviz) {
    this->visualizer = graphviz;
}

GraphVisualizer* DiGraph::getVisualizer() {
    return visualizer;
}


#endif //OOA_PR5_DIGRAPH_H
