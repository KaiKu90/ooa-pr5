//
// Created by Kai Kuhlmann on 04.05.16.
//

#ifndef OOA_PR5_EDGE_H
#define OOA_PR5_EDGE_H

using namespace std;

class Node;

class Edge {

public:
    virtual float getWeight() = 0;
    virtual Node* getStartNode() = 0;
    virtual Node* getEndNode() = 0;
    virtual map<string, string> getAttributes() = 0;
    virtual string getSituation() = 0;

    virtual void setWeight(float weight) = 0;
    virtual void setStartNode(Node *node) = 0;
    virtual void setEndNode(Node *node) = 0;
    virtual void addAttribute(string key, string value) = 0;
};


#endif //OOA_PR5_EDGE_H
