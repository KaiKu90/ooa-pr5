//
// Created by Kai Kuhlmann on 23.06.16.
//

#include <string>
#include <map>
#include "Edge.h"

#ifndef OOA_PR5_LINE_H
#define OOA_PR5_LINE_H

using namespace std;

class Node;
class Line : public Edge {
public:
    Node *start;
    Node *end;
    float weight;
    string situation;
    map<string, string> attributes;

    Line(Node *start, Node *end, float weight) {
        this->start = start;
        this->end = end;
        this->weight = weight;
    }

    float getWeight() {
        return this->weight;
    }

    Node *getStartNode() {
        return this->start;
    }

    Node *getEndNode() {
        return this->end;
    }

    string getSituation() {
        return this->situation;
    }

    map<string, string> getAttributes() {
        return this->attributes;
    }

    void setWeight(float weight) {
        this->weight = weight;
    }

    void setStartNode(Node *start) {
        this->start = start;
    }

    void setEndNode(Node * end) {
        this->end = end;
    }

    void addAttribute(string key, string value) {
        this->attributes[key] = value;
    }
};

#endif //OOA_PR5_LINE_H
