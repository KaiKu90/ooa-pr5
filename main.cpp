#include <iostream>
#include "Graph/DiGraph.h"
#include "Visualizer/OpenCVGraphVisualizer.h"
#include "Graph/Navigator.h"
#include "Vehicle/Car.h"
#include "Decorator/Shortcut.h"
#include "Decorator/Traffic.h"

using namespace std;

void createDummyGraph(DiGraph &g) {
    g.addNode(new Node("Aachen", 100, 600));
    g.addNode(new Node("Berlin", 300, 650));
    g.addNode(new Node("Koeln", 300, 300));
    g.addNode(new Node("Essen", 900, 300));
    g.addNode(new Node("Bonn", 300, 150));
    g.addNode(new Node("Krefeld", 100, 160));

    g.addEdge(new Shortcut(new Line(g.getNode("Bonn"), g.getNode("Essen"), 40)));
    g.addEdge(new Traffic(new Line(g.getNode("Koeln"), g.getNode("Aachen"), 10)));

    g.addEdge("Koeln", "Berlin", 10);
    g.addEdge("Koeln", "Essen", 30);
    g.addEdge("Berlin", "Aachen", 5);
    g.addEdge("Aachen", "Krefeld", 20);
    g.addEdge("Berlin", "Essen", 50);
    g.addEdge("Krefeld", "Bonn", 5);
    g.addEdge("Bonn", "Koeln", 5);
}

int main() {
    DiGraph graph;
    graph.setVisualizer(new OpenCVGraphVisualizer());

    createDummyGraph(graph);

    graph.getVisualizer()->render(graph);

    Navigator navigator(&graph);
    navigator.setVehicle(new Car(120));

    vector<Node*> tour;
    tour.push_back(graph.getNode("Koeln"));
    tour.push_back(graph.getNode("Aachen"));
    tour.push_back(graph.getNode("Bonn"));
    tour.push_back(graph.getNode("Essen"));

    navigator.planTour(tour);

    //cout << "Fahrzeit: " << navigator.planRoute(graph.getNode("Koeln"), graph.getNode("Essen")) << " Minuten" << endl;

    graph.getVisualizer()->show();

    return 0;
}