//
// Created by Kai Kuhlmann on 23.06.16.
//

#ifndef OOA_PR5_DECORATOR_H
#define OOA_PR5_DECORATOR_H

#include <string>
#include <map>
#include "../Graph/Edge.h"

using namespace std;

class Decorator: public Edge {

private:
    Edge *edge;

public:
    Decorator(Edge *edge) {
        this->edge = edge;
    }

    virtual float getWeight() {
        return this->edge->getWeight();
    }

    virtual string getSituation() {
        return this->edge->getSituation();
    }

    virtual Node *getStartNode() {
        return this->edge->getStartNode();
    }

    virtual Node *getEndNode() {
        return this->edge->getEndNode();
    }

    virtual map<string, string> getAttributes() {
        return this->edge->getAttributes();
    }

    virtual void setWeight(float w) {
        this->edge->setWeight(w);
    }

    virtual void setStartNode(Node * n) {
        this->edge->setStartNode(n);
    }

    virtual void setEndNode(Node * n) {
        this->edge->setEndNode(n);
    }

    virtual void addAttribute(string key, string value) {
        this->edge->addAttribute(key, value);
    }
};

#endif //OOA_PR5_DECORATOR_H
