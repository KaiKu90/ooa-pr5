//
// Created by Kai Kuhlmann on 23.06.16.
//

#ifndef OOA_PR5_SHORTCUT_H
#define OOA_PR5_SHORTCUT_H

#include "Decorator.h"

class Shortcut : public Decorator {

private:
    int weight = -6;
    string situation = "Shortcut";

public:
    Shortcut(Edge *edge) : Decorator(edge) {
        this->addAttribute("Shortcut", to_string(this->weight * -1) + " km");
    }

    virtual float getWeight() {
        return Decorator::getWeight() + this->weight;
    }

    virtual string getSituation() {
        return Decorator::getSituation() + ", " + this->situation;
    }
};

#endif //OOA_PR5_SHORTCUT_H
