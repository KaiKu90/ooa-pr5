//
// Created by Kai Kuhlmann on 23.06.16.
//

#ifndef OOA_PR5_TRAFFIC_H
#define OOA_PR5_TRAFFIC_H

#include "Decorator.h"

class Traffic : public Decorator {

private:
    int weight = 9;
    string situation = "Traffic Jam";

public:
    Traffic(Edge *edge) : Decorator(edge) {
        this->addAttribute("Traffic", to_string(this->weight) + " km");
    }

    virtual float getWeight() {
        return Decorator::getWeight() + this->weight;
    }

    virtual string getSituation() {
        return Decorator::getSituation() + ", " + this->situation;
    }
};

#endif //OOA_PR5_TRAFFIC_H
